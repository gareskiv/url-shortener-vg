# URL Shortener Readme

### Welcome to the URL Shortener project! This application simplifies the process of creating short URLs for long web addresses. Follow these steps to set up and run the application on your local machine:

**1. Clone The Repository**
    - git clone https://gitlab.com/gareskiv/url-shortener-vg.git

**2. Import The Database**
    - Find the MySQL database dump file in the "db-dumps" folder located at the root of the application.
    - Import this dump into your MySQL database.

**3. Configure Environment**
    - Duplicate .env-example and rename it to .env if it doesn't already exist.
    - Update .env with your MySQL database credentials.
    
**4. Install Dependencies**
    - npm install
    - composer install

**5. Run The Application**
    - Frontend: npm run dev
    - Backend: php artisan serve