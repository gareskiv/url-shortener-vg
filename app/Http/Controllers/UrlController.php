<?php

namespace App\Http\Controllers;

use App\Models\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\ShortenUrlRequest;
use Exception;
use Illuminate\Support\Facades\Redirect;

class UrlController extends Controller
{   
    public function shorten(ShortenUrlRequest $request)
    {   
        // Check if the URL is safe
        if (!$this->isSafeUrl($request->url)) {
            return response()->json(['error' => 'The URL is not safe to shorten.'], 400);
        }

        // Check if the URL already exists
        $existingUrl = Url::where('original_url', $request->url)->first();

        if ($existingUrl) {
            // If URL exists, return the existing short URL
            return response()->json(['unique_hash' => url($existingUrl->unique_hash), 'hash' => $existingUrl->unique_hash]);
        } else {
            // Generate a new short URL
            $sffledStr= str_shuffle('abscdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_-+');
            $uniqueString = substr(md5(time() . $sffledStr), 0, 6);

            $url = Url::create([
                'original_url' => $request->url,
                'unique_hash' => $uniqueString
            ]);
            return response()->json(['unique_hash' => url($url->unique_hash), 'hash' => $url->unique_hash]);
        }
    }

    public function redirect($shortUrl)
    {   
        try {
            $url = Url::where('unique_hash', $shortUrl)->firstOrFail();
            // Redirect is blocked by the CORS if with commented code below (couldn't find a solution so I'm doing redirect on the frontend).
            // return Redirect::away($url->original_url);
            return response()->json(['original_url' => $url->original_url]);
        }catch(Exeption $e){
            return response()->json(['error' => 'Something bad happened.'], 500);
        }
       
    }

    private function isSafeUrl($url) {
        $apiKey = env('GOOGLE_SAFE_BROWSING_API_KEY');
        $googleSafeBrowsingUrl = 'https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' . $apiKey;

        $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($googleSafeBrowsingUrl, [
            'client' => [
                'clientId' => "URL-shortener-VG",
                'clientVersion' => "1.5.2"
            ],
            'threatInfo' => [
                'threatTypes' => ["MALWARE", "SOCIAL_ENGINEERING"],
                'platformTypes' => ["ANY_PLATFORM"],
                'threatEntryTypes' => ["URL"],
                'threatEntries' => [
                    ['url' => $url]
                ]
            ]
        ]);

        if ($response->failed()) {
            return response()->json(['error' => 'Failed to verify URL safety.'], 500);
        }

        $result = $response->json();

        return empty($result['matches']);
    }
}
