import { createRouter, createWebHistory } from "vue-router";
import axios from 'axios';

const routes = [
    {
      path: "/:catchAll(.*)",
      name: "NotFound",
      component: () => import("../components/PageNotFound.vue"),
      meta: {
        requiresAuth: false
      }
    },
    {
        path: "/",
        name: "Home",
        component: () => import("../components/Home.vue"),
    },
    {
      path: '/:hash',
      name: 'Redirect',
      beforeEnter: async (to, from, next) => {
        const hash = to.params.hash;
        try {
          const response = await axios.get(`/api/${hash}`);

          if (response.status === 200) {
            window.location.href = response.data.original_url;
          }
        } catch (error) {
          if (error.response && error.response.status === 422) {
            console.log(error.response.data.message);
          } else {
            console.log('An unexpected error occurred.');
          }
          next({name: 'Home'}); // Prevent navigation to the route
        }
      }
    }
];

export default createRouter({
    history: createWebHistory(),
    routes,
});