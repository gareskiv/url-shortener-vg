import { createApp } from "vue";
import router from "./router/router";
import App from "./App.vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faArrowLeft, faUserSecret, faLink } from '@fortawesome/free-solid-svg-icons';

/* add icons to the library, so you can use them in the components*/
library.add(faUserSecret, faArrowLeft, faLink)

createApp(App)
    .use(router)
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app')